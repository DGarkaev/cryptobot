﻿using System;
using System.Windows.Forms;
using System.IO;


//IWebElement element = (IWebElement) ((IJavaScriptExecutor)driver).ExecuteScript("return $('.cheese')[0]");

namespace crypto_bot
{
    public partial class frmMain : Form
    {
        private TelegramBot tBot;

        public frmMain()
        {
            InitializeComponent();
            string strCompTime = cbot.Properties.Resources.BuildDate;
            Text = "Дата сборки: "+strCompTime.Substring(0,strCompTime.IndexOf(","));
            btnLoadSettings();
            tBot = new TelegramBot(this);
            tBot._start();
        }
       
        public void toLog(String str)
        {
            if (lstLog.Items.Count > 100)
            {
                lstLog.Items.RemoveAt(0);
            }
            lstLog.Items.Add(str);
            lstLog.TopIndex = lstLog.Items.Count - 1;
            Application.DoEvents();
        }

       
        private void btnLoadSettings()
        {
            txtSettings.Lines = File.ReadAllLines("cryptobot.conf");
        }
        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            File.WriteAllText("cryptobot.conf", txtSettings.Text);
        }

        private void chbTop_CheckedChanged(object sender, EventArgs e)
        {
            this.TopMost = chbTop.Checked;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            tBot._close();
        }

        private void btnUpdateBTC_Click(object sender, EventArgs e)
        {            
            tBot._do_rate();         
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            tBot._close();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (timer1.Enabled)
                label1.Text = "РАБОТА";
            else
                label1.Text = "ОЖИДАНИЕ";
        }

        private void timer1_Tick(object sender, EventArgs e)
        {            
            timer1.Enabled = false;
            tBot._do_rate();
            DateTime localDate = DateTime.Now;
            label2.Text = "Последнее обновление: " + localDate.ToString();
            timer1.Enabled = checkBox1.Checked;
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void btnstart_Click(object sender, EventArgs e)
        {
            tBot._start();
        }

        private void btnOpenBot_Click(object sender, EventArgs e)
        {
            tBot._open_bot("@BTC_CHANGE_BOT");
        }

        private void btn_check_panel_control_Click(object sender, EventArgs e)
        {
            tBot._get_panel_control();
        }

        private void _press_button_exchange_Click(object sender, EventArgs e)
        {
            tBot._press_button_exchange("Обмен");
        }

        private void _press_button_buy_sell_Click(object sender, EventArgs e)
        {
            tBot._press_button_buy_sell(1);
        }

        private void _get_rate_Click(object sender, EventArgs e)
        {
            tBot._get_rate("Сбербанк:ВТБ24");
        }
    }
}

