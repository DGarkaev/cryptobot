﻿namespace crypto_bot
{
    partial class frmMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMain = new System.Windows.Forms.TabPage();
            this._get_rate = new System.Windows.Forms.Button();
            this._press_button_buy_sell = new System.Windows.Forms.Button();
            this._press_button_exchange = new System.Windows.Forms.Button();
            this.btn_check_panel_control = new System.Windows.Forms.Button();
            this.btnOpenBot = new System.Windows.Forms.Button();
            this.btnstart = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnUpdateBTC = new System.Windows.Forms.Button();
            this.chbTop = new System.Windows.Forms.CheckBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtSettings = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.lstLog = new System.Windows.Forms.ListBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabControl1.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabMain);
            this.tabControl1.Controls.Add(this.tabSettings);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(475, 311);
            this.tabControl1.TabIndex = 0;
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this._get_rate);
            this.tabMain.Controls.Add(this._press_button_buy_sell);
            this.tabMain.Controls.Add(this._press_button_exchange);
            this.tabMain.Controls.Add(this.btn_check_panel_control);
            this.tabMain.Controls.Add(this.btnOpenBot);
            this.tabMain.Controls.Add(this.btnstart);
            this.tabMain.Controls.Add(this.checkBox1);
            this.tabMain.Controls.Add(this.label2);
            this.tabMain.Controls.Add(this.label1);
            this.tabMain.Controls.Add(this.btnUpdateBTC);
            this.tabMain.Controls.Add(this.chbTop);
            this.tabMain.Controls.Add(this.btnClose);
            this.tabMain.Location = new System.Drawing.Point(4, 22);
            this.tabMain.Name = "tabMain";
            this.tabMain.Padding = new System.Windows.Forms.Padding(3);
            this.tabMain.Size = new System.Drawing.Size(467, 285);
            this.tabMain.TabIndex = 0;
            this.tabMain.Text = "Основное";
            this.tabMain.UseVisualStyleBackColor = true;
            // 
            // _get_rate
            // 
            this._get_rate.Location = new System.Drawing.Point(289, 163);
            this._get_rate.Name = "_get_rate";
            this._get_rate.Size = new System.Drawing.Size(148, 23);
            this._get_rate.TabIndex = 24;
            this._get_rate.Text = "_get_rate";
            this._get_rate.UseVisualStyleBackColor = true;
            this._get_rate.Click += new System.EventHandler(this._get_rate_Click);
            // 
            // _press_button_buy_sell
            // 
            this._press_button_buy_sell.Location = new System.Drawing.Point(289, 134);
            this._press_button_buy_sell.Name = "_press_button_buy_sell";
            this._press_button_buy_sell.Size = new System.Drawing.Size(148, 23);
            this._press_button_buy_sell.TabIndex = 23;
            this._press_button_buy_sell.Text = "_press_button_buy_sell";
            this._press_button_buy_sell.UseVisualStyleBackColor = true;
            this._press_button_buy_sell.Click += new System.EventHandler(this._press_button_buy_sell_Click);
            // 
            // _press_button_exchange
            // 
            this._press_button_exchange.Location = new System.Drawing.Point(289, 105);
            this._press_button_exchange.Name = "_press_button_exchange";
            this._press_button_exchange.Size = new System.Drawing.Size(148, 23);
            this._press_button_exchange.TabIndex = 22;
            this._press_button_exchange.Text = "_press_button_exchange";
            this._press_button_exchange.UseVisualStyleBackColor = true;
            this._press_button_exchange.Click += new System.EventHandler(this._press_button_exchange_Click);
            // 
            // btn_check_panel_control
            // 
            this.btn_check_panel_control.Location = new System.Drawing.Point(289, 76);
            this.btn_check_panel_control.Name = "btn_check_panel_control";
            this.btn_check_panel_control.Size = new System.Drawing.Size(148, 23);
            this.btn_check_panel_control.TabIndex = 21;
            this.btn_check_panel_control.Text = "_check_panel_control";
            this.btn_check_panel_control.UseVisualStyleBackColor = true;
            this.btn_check_panel_control.Click += new System.EventHandler(this.btn_check_panel_control_Click);
            // 
            // btnOpenBot
            // 
            this.btnOpenBot.Location = new System.Drawing.Point(289, 47);
            this.btnOpenBot.Name = "btnOpenBot";
            this.btnOpenBot.Size = new System.Drawing.Size(148, 23);
            this.btnOpenBot.TabIndex = 20;
            this.btnOpenBot.Text = "_open_bot";
            this.btnOpenBot.UseVisualStyleBackColor = true;
            this.btnOpenBot.Click += new System.EventHandler(this.btnOpenBot_Click);
            // 
            // btnstart
            // 
            this.btnstart.Location = new System.Drawing.Point(289, 13);
            this.btnstart.Name = "btnstart";
            this.btnstart.Size = new System.Drawing.Size(148, 23);
            this.btnstart.TabIndex = 19;
            this.btnstart.Text = "_start";
            this.btnstart.UseVisualStyleBackColor = true;
            this.btnstart.Click += new System.EventHandler(this.btnstart_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 47);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(73, 17);
            this.checkBox1.TabIndex = 18;
            this.checkBox1.Text = "Работать";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Последнее обновление:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "...";
            // 
            // btnUpdateBTC
            // 
            this.btnUpdateBTC.Location = new System.Drawing.Point(289, 227);
            this.btnUpdateBTC.Name = "btnUpdateBTC";
            this.btnUpdateBTC.Size = new System.Drawing.Size(75, 23);
            this.btnUpdateBTC.TabIndex = 15;
            this.btnUpdateBTC.Text = "Обновить";
            this.btnUpdateBTC.UseVisualStyleBackColor = true;
            this.btnUpdateBTC.Click += new System.EventHandler(this.btnUpdateBTC_Click);
            // 
            // chbTop
            // 
            this.chbTop.AutoSize = true;
            this.chbTop.Location = new System.Drawing.Point(6, 6);
            this.chbTop.Name = "chbTop";
            this.chbTop.Size = new System.Drawing.Size(99, 17);
            this.chbTop.TabIndex = 13;
            this.chbTop.Text = "Всегда сверху";
            this.chbTop.UseVisualStyleBackColor = true;
            this.chbTop.CheckedChanged += new System.EventHandler(this.chbTop_CheckedChanged);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(289, 256);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "_close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.panel2);
            this.tabSettings.Controls.Add(this.panel1);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Size = new System.Drawing.Size(467, 285);
            this.tabSettings.TabIndex = 3;
            this.tabSettings.Text = "Настройки";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtSettings);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 29);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(467, 256);
            this.panel2.TabIndex = 2;
            // 
            // txtSettings
            // 
            this.txtSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSettings.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.txtSettings.Location = new System.Drawing.Point(0, 0);
            this.txtSettings.Multiline = true;
            this.txtSettings.Name = "txtSettings";
            this.txtSettings.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtSettings.Size = new System.Drawing.Size(467, 256);
            this.txtSettings.TabIndex = 1;
            this.txtSettings.WordWrap = false;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.btnSaveSettings);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 29);
            this.panel1.TabIndex = 1;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Location = new System.Drawing.Point(3, 3);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(75, 23);
            this.btnSaveSettings.TabIndex = 0;
            this.btnSaveSettings.Text = "Сохранить";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // lstLog
            // 
            this.lstLog.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstLog.FormattingEnabled = true;
            this.lstLog.HorizontalScrollbar = true;
            this.lstLog.Location = new System.Drawing.Point(0, 0);
            this.lstLog.Name = "lstLog";
            this.lstLog.Size = new System.Drawing.Size(475, 155);
            this.lstLog.TabIndex = 1;
            // 
            // timer1
            // 
            this.timer1.Interval = 60000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.lstLog);
            this.splitContainer1.Size = new System.Drawing.Size(475, 470);
            this.splitContainer1.SplitterDistance = 311;
            this.splitContainer1.TabIndex = 2;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(475, 470);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Рабочее место оператора";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.tabControl1.ResumeLayout(false);
            this.tabMain.ResumeLayout(false);
            this.tabMain.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabSettings.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabMain;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtSettings;
        private System.Windows.Forms.CheckBox chbTop;
        private System.Windows.Forms.ListBox lstLog;
        private System.Windows.Forms.Button btnUpdateBTC;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button btnstart;
        private System.Windows.Forms.Button btnOpenBot;
        private System.Windows.Forms.Button btn_check_panel_control;
        private System.Windows.Forms.Button _press_button_exchange;
        private System.Windows.Forms.Button _press_button_buy_sell;
        private System.Windows.Forms.Button _get_rate;
    }
}

