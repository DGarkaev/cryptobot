﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace crypto_bot
{
    public static class Scanf
    {
        /// <summary>
        /// This method is the inverse of <see cref="String.Format"/>.
        /// </summary>
        /// <param name="value">The string to format.</param>
        /// <param name="format">The format string to parse.</param>
        /// <returns>The parsed values.</returns>
        public static IEnumerable<string> Parse(this string value, string format, StringComparison comparison = StringComparison.Ordinal)
        {
            var doubledMatches = new Regex(@"(\{\d+\}\{\d+\})");
            if (doubledMatches.IsMatch(format))
            {
                throw new ArgumentException("Invalid format string.  You must put at least one character between all parse tokens.");
            }
            var emptyBraces = new Regex(@"(\{\})");
            if (emptyBraces.IsMatch(format))
            {
                throw new ArgumentException("Do not include {} in your format string.");
            }
            var expression = new Regex(@"(\{\d+\})+");
            var matches = expression.Matches(format);
            var boundaries = format.Split(matches).ToArray();
            var startPosition = 0;
            for (int i = 0; i < matches.Count; ++i)
            {
                startPosition += boundaries[i].Length;
                var nextBoundary = boundaries[i + 1];
                if (string.IsNullOrEmpty(nextBoundary))
                {
                    yield return value.Substring(startPosition);
                }
                else
                {
                    var nextBoundaryStartIndex = value.IndexOf(nextBoundary, startPosition, comparison);
                    var parsedLength = nextBoundaryStartIndex - startPosition;
                    var parsedValue = value.Substring(startPosition, parsedLength);
                    startPosition += parsedValue.Length;
                    yield return parsedValue;
                }
            }
        }
        public static IEnumerable<string> Split(this string source, MatchCollection matches)
        {
            int index = 0;
            foreach (var match in matches.Cast<Match>())
            {
                yield return source.Substring(index, match.Index - index);
                index = match.Index + match.Length;
            }
            yield return source.Substring(index);
        }
    }
}
