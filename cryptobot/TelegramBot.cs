﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IniParser;
using IniParser.Model;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using System.Threading;
using System.Diagnostics;

namespace crypto_bot
{
    class Rates
    {

        private string time; //datetime время получения котировок
        private string currency; //имя криптовалюты
        private string direct; //способ обмена
        private string mode; //купля/продажа
        private string rate; //котировка
        private string minval; //мин объем
        private string maxval; //макс объем
        private string nametraider; //имя трейдера

        public string Time { get => time; set => time = value; }
        public string Currency { get => currency; set => currency = value; }
        public string Direct { get => direct; set => direct = value; }
        public string Mode { get => mode; set => mode = value; }
        public string Rate { get => rate; set => rate = value; }
        public string Minval { get => minval; set => minval = value; }
        public string Maxval { get => maxval; set => maxval = value; }
        public string Nametraider { get => nametraider; set => nametraider = value; }

        private String q(String s) { return "'" + s + "'"; }
        public Rates()
        {
            DateTime dt = DateTime.Now;
            this.time = dt.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public Rates(string currency, string direct, string mode, string rate, string minval, string maxval, string nametraider)
        {
            DateTime dt = DateTime.Now;
            this.time = dt.ToString("");
            this.Currency = q(currency);
            this.Direct = q(direct);
            this.Mode = q(mode);
            this.Rate = q(rate);
            this.Minval = q(minval);
            this.Maxval = q(maxval);
            this.Nametraider = q(nametraider);
        }

        public override String ToString()
        {
            List<String> l = new List<string>();
            l.Add(q(time));
            l.Add(q(Currency));
            l.Add(q(Direct));
            l.Add(q(Mode));
            l.Add(q(Rate));
            l.Add(q(Minval));
            l.Add(q(Maxval));
            l.Add(q(Nametraider));
            return String.Join(",", l);
        }
    };

    class TelegramBot
    {
        /*
         этапы работы:
         - запуск chrome
         - получить ссылки на элементы Telegram:
            - фрейм контактов
            - фрейм сообщений
        - найти бота по имени
             */
        IWebDriver _chrome = null;

        //текст на последней кнопки на фрейме обмена
        string _lastDirectButtonText = "";

        List<Rates> _rr = new List<Rates>();

        //счетчик фрейма
        int _currentFrame = 0;

        public void _start()
        {            
            toLog("Запуск Crome.");
            if (_chrome != null)
            {
                toLog("Chrome уже запущен");
                return;
            }

            KillProcess();
            
            ChromeOptions options = new ChromeOptions();
            options.AddArguments("user-data-dir=" + getValue("Chrome", "userdata"));
            options.AddArguments(getValue("Chrome", "arg"));

            var chromeDriverService = ChromeDriverService.CreateDefaultService();
            chromeDriverService.HideCommandPromptWindow = true;
            _chrome = new ChromeDriver(chromeDriverService, options);
            String url = getValue("Telegram", "url");
            toLog("Переход на web версию Telegram '" + url + "'");
            try
            {
                _chrome.Navigate().GoToUrl(url);
            }
            catch (Exception e)
            {
                toLog("Ошибка запуска Crome.");
                toLog("ERROR: " + e.Message);
                toLog("Выход.");
                _chrome = null;
                KillProcess();
                return;
            }
            _set_wait(5);
            //ждем подключения
            while(true)
            {
                toLog("Ожидание подкллючения к Telegram.");
                _pause(1);
                var con = _chrome.FindElements(By.ClassName("tg_head_split"));
                if (con.Count == 0) continue;
                if (con[0].Text.Contains("Connect")) continue;
                break;
            }
            toLog("Подкллючение к Telegram установленно.");
        }

        //получим элементы бота
        public void _get_main_element()
        {
        }

        //Поиск бота и его активация
        public bool _open_bot(string botname)
        {
            _currency_name = getValue(_sec, "currency.name");// "BTC";
            
            //найдем бота
            toLog("Поиск поля 'ПОИСК КОНТАКТА'.");
            IWebElement find;
            try
            {
                find = _chrome.FindElement(By.ClassName("im_dialogs_panel"))
                                .FindElement(By.TagName("input"));
            }
            catch (Exception e)
            {
                toLog("Не найдено поле поиска контакта.");
                toLog("ERROR: " + e.Message);
                return false;
            }
            find.Clear();
            toLog("Поле 'ПОИСК КОНТАКТА' найдено.");
            toLog("Поиск бота '" + botname + "'.");
            //------------------------------------------------------------------------
            //String val = getValue("BTC", "bot.name");
            find.SendKeys(botname);
            //------------------------------------------------------------------------
            //кликнем на него            
            ReadOnlyCollection<IWebElement> dlg;
            try
            {
                dlg = _chrome.FindElement(By.ClassName("im_dialogs_col")).FindElements(By.ClassName("im_dialog_peer"));
                if (dlg.Count() == 0)
                {
                    toLog("Бот не найден. Проверте настройки.");
                    return false;
                }
            }
            catch (Exception e)
            {
                toLog("Не могу найти панель с контактами.");
                toLog("ERROR: " + e.Message);
                return false;
            }
            toLog("Активируем бота '" + botname + "'.");
            //------------------------------------------------------------------------
            //т.к. ищем бота но его логину, то он должен быть первый в списке
            dlg[0].Click();
            //------------------------------------------------------------------------
            return true;
        }

        //Активация панели управления
        public IWebElement _get_panel_control()
        {
            toLog("Поиск панели управления.");
            IWebElement controlPanel = null;
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    controlPanel = _chrome.FindElement(By.ClassName("im_send_keyboard_wrap"));
                    toLog("Панель управления найдена.");
                    break;
                }
                catch (Exception e)
                {
                    toLog("ERROR: " + e.Message);
                    toLog("Не найдена панель управления. Пробуем послать сообщение.");
                    if (!_send_msg_to_bot())
                    {
                        return null;
                    }
                    _pause(3);
                    toLog("Пробуем найти панель управления еще раз.");
                    continue;
                }
            }
            if (controlPanel == null)
            {
                toLog("Панель управления не найдена.");
            }
            return controlPanel;
        }

        public bool _do_rate()
        {
            bool rz = false;
            List<string> sections = new List<string>(getValue("Settings", "sections").Split(':'));

            foreach (string _s in sections)
            {
                _sec = _s;
                toLog(string.Format("Секция: [{0}]",_sec));
                _open_bot(getValue(_sec,"bot.name"));

                for (int i = 0; i <= 1; i++)
                {
                    rz = _press_button_exchange(getValue(_sec, "btn.exchange"));
                    rz = _press_button_buy_sell(i);
                    rz = _get_rate(getValue(_sec, "direct"));
                }
            }
            return true;
        }
        //Нажимаем кнопку "Обмен ХХХ"
        public bool _press_button_exchange(string ex)
        {
            //Нажмем на кнопку 'Обмен ХХХ'
            //val = getValue("BTC", "str.exchange");
            try
            {
                IWebElement btnSell = _get_panel_control().FindElement(By.XPath(".//button[contains(text(), '" + ex + "')]"));
                btnSell.Click();
            }
            catch (Exception e)
            {
                toLog("Ошибка поиска или нажатия кнопки '" + ex + "'.");
                toLog("ERROR: " + e.Message);
                return false;
            }
            toLog("Нажатие кнопки '" + ex + "'.");
            return true;
        }

        //Если сигнатура сообщения соответствует шаблону, возвращаем этот элемент
        public IWebElement _find_signature(string id)
        {
            IWebElement _l = null;
            int j = 10;
            //Ищем сигнатуру в сообщении
            for (int i = 0; i < j; i++)
            {
                _l = _get_last_msg();
                if (_l.Text.Contains(id))
                {
                    toLog("Найдена сигнатура '" + id + "'");
                    break;
                }
                toLog("Не найдено сообщение с сигнатурой '" + id + "'.");
                _l = null;
                _pause(3);
                toLog(string.Format("[{0}] Пробуем еще раз.", j - i));
            }
            if (_l == null)
            {
                toLog("Не найдено сообщение с сигнатурой '" + id + "'.");
            }
            return _l;
        }

        //Нажимаем кнопку КУПИТЬ/ПРОДАТЬ
        public bool _press_button_buy_sell(int mbs)
        {
            _mbs = mbs;
            string bs = mbs == 0 ? getValue(_sec, "btn.buy") : getValue(_sec, "btn.sell");// "Купить" : "Продать";

            //Ищем сигнатуру в сообщении
            string id = getValue(_sec,"id.buy.sell");
            IWebElement _l = _find_signature(id);
            if (_l == null)
            {                
                    return false;
            }
            //найдем кнопку и нажмем на неё
            try
            {
                toLog("Нажатие кнопки '" + bs + "'");
                IWebElement _b = _l.FindElement(By.XPath(".//button[contains(text(), '" + bs + "')]"));
                _b.Click();
            }
            catch (Exception e)
            {
                toLog("Ошибка нажатия кнопки '" + bs + "'");
                toLog("ERROR: " + e.Message);
                return false;
            }
            return true;
        }

        //Ожидание смены фрейма по изменению надписи на последней кнопке
        public bool _wait_change_button_text(string current_text)
        {
            IWebElement _el = null;
            IWebElement _lmsg = null;
            for (int j = 0; j < 5; j++)
            {
                //поиск последней кнопки
                for (int i = 0; i < 5; i++)
                {
                    _lmsg = _get_last_msg();
                    if (_lmsg == null) return false;
                    try
                    {
                        _el = _lmsg.FindElements(By.TagName("button")).Last();
                        break;
                    }
                    catch (Exception e)
                    {
                        toLog("Ошибка поиска последней кнопки.");
                        toLog(e.Message);
                        _pause(3);
                        continue;
                    }
                }
                if (_el == null)
                {
                    toLog("Не удалось получить последнюю кнопку во фрейме");
                    return false;
                }
                string _txt = _el.Text;
                if (_txt != current_text)
                {
                    toLog("Обнаружено изменение фрейма.");
                    return true;
                }
                toLog("Изменение фрейма не обнаружено.Пробуем еще раз.");
                _pause(3);
            }
            toLog("Изменение фрейма не обнаружено.");
            return false;
        }

        //Собираем котировки
        //Направления задаются списком
        public bool _get_rate(string direct)
        {
            try
            {
                List<string> direct_list = new List<string>(direct.Split(':'));
                _rr.Clear();

                //пройдем по всем фреймам направления обмена
                _currentFrame = 1;
                //toLog(string.Format("----------\n_frame={0}\n----------",_currentFrame));
                do//обход фреймов с котировками
                {
                    //текущее направление на фрейме
                    var _currentDirect = 0;

                    do//обход котировок
                    {
                        //проверим что сигнатура соответствует фрейму с направлениями обмена
                        //сигнатура сообщения с направлениями обмена
                        string id = getValue(_sec, "id.direct");// "Выберите удобный";
                        IWebElement _l = null;
                        for (int i = 0; i < 2; i++)
                        {
                            _l = _find_signature(id);
                            if (_l != null) break;
                            _pause(30);
                        }

                        if (_l == null) return false;
                        //-------------------------------------------------------------------

                        //переключимся на нужный фрейм                    
                        _l = _goto_frame();
                        var _direct = _l.FindElements(By.TagName("button"));

                        if (_direct == null) return false;

                        //Количество направлений обмена (3 последние кнопки - управляющие)
                        var _directCount = _direct.Count - 3;

                        if (_directCount <= 0)
                        {
                            toLog("Не найдены направления обмена");
                            return false;
                        }
                        toLog(String.Format("Найдены {0} направлений обмена", _directCount));
                        toLog(String.Format("Текущее направление {0}", _currentDirect));

                        //запомним текст последней кнопки
                        _lastDirectButtonText = _direct.Last().Text;

                        //------------------------------------------------------------------------------------
                        //если список направление есть в списке direct_list, то
                        //получаем котировки и удаляем это направление из списка
                        string _direct_name = _direct[_currentDirect].Text;
                        if (_direct_presents(ref direct_list, _direct_name))
                        {
                            //флаг сбора котировок
                            //das_rate = true;

                            //нажимаем кнопку направления обмена
                            toLogN("Нажимаем '" + _direct[_currentDirect].Text + "'");
                            _direct[_currentDirect].Click();
                            //зафиксируем время сбора котировок
                            string time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                            _pause(3);
                            //если не появилась сигнатура - выход
                            var id_rate = getValue(_sec, "id.rate");// "Это список из";
                            var _d = _find_signature(id_rate);
                            if (_d == null) return false;

                            //------------------------------------------------
                            //обрабатываем фрейм с котировками
                            //получим кнопки с котировками
                            var _rate = _d.FindElements(By.TagName("button"));
                            var _rateCount = _rate.Count - 3;
                            if (_rate.Count <= 0)
                            {
                                toLog("Не найдены котировки");
                                return false;
                            }

                            string _direct_name_norm = "xxx";// = Regex.Split(_direct_name, @"\d+")[0];
                                                             //Regex r = new Regex(@"^(:.+:\s)(?<dn>.+)(\s\d+)(.+)");
                            Regex r = new Regex(@"^(?(:)(:.+:\s))(?<dn>.+\s)(\d+)");
                            Match m = r.Match(_direct_name);
                            if (m.Success)
                            {
                                Console.WriteLine(r.Match(_direct_name).Result("${dn}"));
                                _direct_name_norm = r.Match(_direct_name).Result("${dn}").Trim();
                            }
                            else
                            {
                                toLog("Ошибка парсинга направления обмена.");
                                return false;
                            }
                            //тут собираем котировки

                            for (int j = 0; j < _rateCount; j++)
                            {
                                //в начале строки удаляем :xxxxx: XXXXXXXXX
                                string _sd = _rate[j].Text.Substring(_rate[j].Text.IndexOf(' ') + 1);
                                _sd = _sd.Replace(" - ", "-");
                                _sd = _sd.Replace('-', ' ').Replace('(', ' ').Replace(')', ' ').Replace("р. ", "").Replace("₽ ", "");
                                _sd = _sd.Replace("  ", " ");
                                var _sl = _sd.Split(' ');
                                if (_sl[1] == "to") _sl[1] = "1";
                                _rr.Add(new Rates()
                                {
                                    Time = time,
                                    Currency = _currency_name,
                                    Direct = _direct_name_norm,
                                    Mode = string.Format("{0}", _mbs),
                                    Rate = _sl[0],
                                    Minval = _sl[1],
                                    Maxval = _sl[2],
                                    Nametraider = _sl[3]
                                });
                            }
                            //посылаем данные на сервер
                            String csv = "data=" + String.Join("\n", _rr);
                            if (!Sent2Server(csv))
                            {
                                toLog("Ошибка отправки данных");
                            }
                            _rr.Clear();

                            //нажимаем предпоследнюю кнопку 'Отмена' во фрейме котировок
                            //или кнопку 'назад'
                            if (_rate[_rate.Count - 1].Text.Contains("Назад"))
                            {
                                toLogN("Нажимаем '" + _rate[_rate.Count - 1].Text + "'");
                                _rate[_rate.Count - 1].Click();
                            }
                            else
                            {
                                toLogN("Нажимаем '" + _rate[_rate.Count - 2].Text + "'");
                                _rate[_rate.Count - 2].Click();
                            }

                            //I - если заданный список пройден, то выход
                            if (direct_list.Count == 0) break;

                            _pause(3);
                        }
                        //---------------------------------------------------------
                        _currentDirect++;
                        //если прошли весь список направлений обмена
                        if (_currentDirect > _directCount - 1) break;
                    }
                    while (true);
                    toLog("----------------------------------------------------------------------------");
                    _currentFrame++;
                    //II - если заданный список пройден, то выход
                    if (direct_list.Count == 0) break;

                    //если все фреймы пройдены - выход
                    if (_lastDirectButtonText[0] == '1') break;
                    //-------------------------------
                    _pause(10);
                } while (true);
                toLog("Котировки собраны.");
                return true;
            }
            catch (Exception e)
            {
                toLog("Необработанная ошибка в _get_rate().");
                toLog(e.Message);
                return false;
            }
        }

        //поиск есть ли текущее направление обмена в нашем списке
        //и если есть - удаляем его
        private bool _direct_presents(ref List<string> direct_list, string direct_name)
        {
            foreach (var e in direct_list)
            {
                if (direct_name.Contains(e))
                {
                    direct_list.Remove(e);
                    return true;
                }
            }
            return false;
        }

        private IWebElement _goto_frame()
        {
            if (_currentFrame == 0) return _get_last_msg();

            IWebElement _l = null;
            IWebElement _e = null;
            string txt;

            // проверим текущий фрейм
            _l = _get_last_msg();
            var _es = _l.FindElements(By.TagName("button"));
            int _ec = _es.Count();
            _e = _es[_ec - 1];

            char[] sp = { '/' };
            var _f = _e.Text.Split(sp);
            int idx = int.Parse(_f[0]) - 1;
            //toLog(string.Format("----------\nidx={0}\n----------", idx));

            for (int i = idx; i < _currentFrame; i++)
            {
                _l = _get_last_msg();
                if (_l == null) return null;
                try
                {
                    _e = _l.FindElements(By.TagName("button")).Last();
                    txt = _e.Text;
                    _e.Click();
                    //_pause(3);
                    _wait_change_button_text(txt);
                }
                catch (Exception e)
                {
                    toLog(e.Message);
                    return null;
                }

            }

            return _l;
        }

        public IWebElement _get_last_msg()
        {
            IWebElement el = _chrome.FindElement(By.ClassName("im_history_messages_peer")).FindElements(By.ClassName("im_history_message_wrap")).Last();
            return el;
        }
        private void _pause(int t)
        {
            Random rnd = new Random();
            int dt = rnd.Next(6);
            toLog(String.Format("Пауза {0} с.", t + dt));
            Thread.Sleep((t + dt) * 1000);
        }
        //-----------------------------------------------------------------------------



        frmMain mainForm;
        FileIniDataParser fileIniData;
        IniData parsedData;

        //текущий режим: 0 - продать, 1 - купить
        private int _mbs;
        private string _currency_name;
        //текущая секция в конфиге
        private string _sec;

        public TelegramBot(frmMain mForm)
        {
            mainForm = mForm;
            fileIniData = new FileIniDataParser();
            fileIniData.Parser.Configuration.CommentString = "#";
            fileIniData.Parser.Configuration.SkipInvalidLines = true;
            //Parse the ini file
            parsedData = fileIniData.ReadFile("cryptobot.conf", Encoding.UTF8);
            //String botNameBTC = parsedData["BTC"]["botname"].Trim('"');
        }
        public String getValue(String sec, String key)
        {
            String rz = parsedData[sec][key];
            if (!IsNullOrEmpty(rz)) rz = rz.Trim('"');//.Trim(' ').Trim('"');
            return rz;
        }

        private bool IsNullOrEmpty(string s)
        {
            return s == null || s == String.Empty;
        }

        void toLog(String s)
        {
            DateTime localDate = DateTime.Now;
            String dt = "[" + localDate.ToString() + "]: ";

            string[] sl = s.Replace("\r", "").Split('\n');
            if (sl.Count() > 1)
            {
                foreach (string si in sl)
                {
                    mainForm.toLog(dt + si);
                }
            }
            else
                mainForm.toLog(dt + s);
        }
        void toLogN(String s)
        {
            DateTime localDate = DateTime.Now;
            String dt = "[" + localDate.ToString() + "]: ";
            mainForm.toLog(dt + s);
        }

        internal void _close()
        {
            toLog("Закрываем сессию");
            if (_chrome != null)
            {
                _chrome.Quit();
                _chrome = null;
            }
        }

        internal bool _send_msg_to_bot()
        {
            //Отправим боту сообщение чтоб появилась панель управления с кнопками
            IWebElement textBox = null;
            try
            {
                textBox = _chrome.FindElement(By.ClassName("im_history_selected_wrap"))
                                 .FindElement(By.ClassName("composer_rich_textarea"));
            }
            catch (NoSuchElementException)
            {

                toLog("Не найдено поле ввода.");
                return false;
            }
            textBox.SendKeys(".");
            //нажмем кнопку отправить
            textBox.SendKeys(OpenQA.Selenium.Keys.Enter);
            toLog("Сообщение боту отправленно.");
            return true;
        }

        private bool Sent2Server(string csv)
        {
            toLog("Отправка на сервер.");
            toLog("Данные:");
            toLog(csv);
            try
            {
                //послать на сервер
                byte[] byteArray = Encoding.UTF8.GetBytes(csv);
                // Create a request for the URL. 		
                WebRequest request = WebRequest.Create("http://mycryptolab.pro:81/botrate/setrate.php");
                request.Method = "POST";
                // Set the ContentType property of the WebRequest.  
                request.ContentType = "application/x-www-form-urlencoded";
                // Set the ContentLength property of the WebRequest.  
                request.ContentLength = byteArray.Length;
                // Get the request stream.  
                Stream dataStream = request.GetRequestStream();
                // Write the data to the request stream.  
                dataStream.Write(byteArray, 0, byteArray.Length);
                // Close the Stream object.  
                dataStream.Close();
                // Get the response.  
                WebResponse response = request.GetResponse();
                // Display the status.  
                toLog(((HttpWebResponse)response).StatusDescription);
                // Get the stream containing content returned by the server.  
                dataStream = response.GetResponseStream();
                // Open the stream using a StreamReader for easy access.  
                StreamReader reader = new StreamReader(dataStream);
                // Read the content.  
                string responseFromServer = reader.ReadToEnd();
                // Display the content.  
                toLog(responseFromServer);
                // Clean up the streams.  
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (Exception)
            {
                toLog("ERROR: При отправке возникла какая-то хрень. Выход.");
                return false;
            }
            toLog("Данные отправленны.");
            return true;
        }

        void _set_wait(int t)
        {
            _chrome.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(t);
        }

        void KillProcess()
        {
            toLog("Убиваем процесс-зобми 'chromedriver.exe'");
            try
            {
                foreach (Process proc in Process.GetProcessesByName("chromedriver"))
                {
                    KillProcessTree(proc);
                }
            }
            catch (Exception ex)
            {
                toLog(ex.Message);
            }
        }
        void KillProcessTree(System.Diagnostics.Process process)
        {
            string taskkill = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.System), "taskkill.exe");
            using (var procKiller = new System.Diagnostics.Process())
            {
                procKiller.StartInfo.FileName = taskkill;
                procKiller.StartInfo.Arguments = string.Format("/PID {0} /T /F", process.Id);
                procKiller.StartInfo.CreateNoWindow = true;
                procKiller.StartInfo.UseShellExecute = false;
                procKiller.Start();
                procKiller.WaitForExit(1000);   // wait 1sec
            }
        }
    }
}
